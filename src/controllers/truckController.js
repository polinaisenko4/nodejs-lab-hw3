/* eslint-disable require-jsdoc */

const Truck = require('../models/truckModel');

class TruckController {
  async getTrucks(req, res) {
    try {
      const limit = parseInt(req.query.limit || '0');
      const offset = parseInt(req.query.offset || '0');

      const truck = await Truck.find({
        created_by: req.user.userId}).select('-__v')
          .skip(offset)
          .limit(limit);

      return res.status(200).send({
        trucks: truck,
      });
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }

  async addTruck(req, res) {
    try {
      const {userId} = req.user;
      const {type} = req.body;

      if (!type) {
        return res.status(400).send({message: 'Add truck'});
      }

      const newTruck = {
        type,
        created_by: userId,
        created_date: Date.now(),
      };
      await Truck.create(newTruck);

      res.status(200).json({message: 'Truck created successfully'});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }

  async getTruckById(req, res) {
    try {
      const id = req.params.id;

      const truck = await Truck.findOne({_id: id}).select('-__v');
      if (truck) {
        res.status(200).send({truck});
      } else {
        res.status(400).send({message: 'Not found'});
      }
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }
  async updateTruckById(req, res) {
    try {
      const {userId} = req.user;
      const id = req.params.id;
      const newTruck = req.body.type;

      await Truck.findOneAndUpdate(
          {_id: id, userId},
          {type: newTruck},
      );

      res.status(200).send({message: 'Truck details changed successfully'});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }

  async deleteTruckById(req, res) {
    try {
      const id = req.params.id;

      await Truck.findByIdAndRemove({_id: id});

      res.status(200).send({message: 'Truck deleted successfully'});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }

  async assignTruckById(req, res) {
    try {
      const id = req.params.id;
      const {userId} = req.user;

      const assignedTruck = await Truck.findOne(
          {created_by: userId, assigned_to: userId},
      );

      if (assignedTruck && assignedTruck.status === 'OL') {
        return res.status(400).send({message: 'Your track space is over'});
      }

      await Truck.findOneAndUpdate(
          {created_by: userId, assigned_to: userId},
          {$set: {assigned_to: null}},
      );

      const truck = await Truck.findOneAndUpdate(
          {_id: id, created_by: userId},
          {$set: {assigned_to: userId}},
      );

      if (!truck) {
        return res.status(400).send({message: 'Truck is not'});
      }

      res.status(200).send({message: 'Truck assigned successfully'});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }
}


module.exports = new TruckController();
