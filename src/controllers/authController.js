/* eslint-disable require-jsdoc */

const jwt = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');
const {User} = require('../models/userModel');
const {authValidator} = require('../helpers/authValidation');


class AuthController {
  async register(req, res) {
    try {
      const {email, password, role} = req.body;
      const {error} = authValidator.validate({email, password, role});
      if (error) {
        return res.status(400)
            .send({message: 'Incorrect data'});
      }
      const newUser = await User.findOne({email});
      if (newUser) {
        return res.status(400)
            .send({message: 'User already exists! Please login!'});
      }
      const user = new User({
        email,
        password: await bcryptjs.hash(password, 10),
        role,
      });
      await user.save();
      res.status(200).send({message: 'Profile created successfully'});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }
  async login(req, res) {
    try {
      const {email, password} = req.body;
      const {error} = authValidator.validate({email, password});
      if (error) {
        return res.status(400)
            .send({message: 'Incorrect data'});
      }
      const user = await User.findOne({email});
      if (!user) {
        return res.status(400)
            .send({message: 'Invalid email'});
      }
      const checkPassword = await bcryptjs.compare(password, user.password);
      if (!checkPassword) {
        return res.status(400)
            .send({message: 'Invalid password'});
      }
      const token = jwt.sign({
        _id: user._id,
        email: user.email,
        role: user.role,
      }, process.env.SECRET_KEY);
      res.status(200).send({jwt_token: token});
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }
  async forgotPassword(req, res) {
    try {
      const {email} = req.body;
      const user = await User.findOne({email});
      if (!user) {
        return res.status(400)
            .send({message: 'Invalid email'});
      }
      res.status(200).send({
        message: 'New password sent to your email address',
      });
    } catch (e) {
      res.status(400).send({message: e.message});
    }
  }
};

module.exports = new AuthController();
