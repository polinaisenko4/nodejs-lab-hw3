const {Router} = require('express');
const router = Router();

const controllerLoads = require('../controllers/loadConroller');
const shipperMiddleware = require('../middlewares/checkShipperMiddleware');
const driverMiddleware = require('../middlewares/checkDriverMiddleware');


router.get('/', controllerLoads.getLoads);
router.post('/', shipperMiddleware, controllerLoads.addLoadByUser);
router.get('/active', driverMiddleware,
    controllerLoads.getActiveLoadersByDriver);
router.post('/:id/post', shipperMiddleware, controllerLoads.addLoadById);
router.get('/:id', controllerLoads.getLoadById);
router.delete('/:id', shipperMiddleware, controllerLoads.deleteLoadById);
router.put('/:id', shipperMiddleware, controllerLoads.updateLoadById);
router.get('/:id/shipping_info', shipperMiddleware,
    controllerLoads.getShippingInfoById);
router.patch('/active/state', driverMiddleware, controllerLoads.updateState);

module.exports = router;
