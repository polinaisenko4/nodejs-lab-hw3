const {Router} = require('express');
const router = Router();
const userController = require('../controllers/userController');


router.get('/me', userController.getUser);
router.delete('/me', userController.deleteUser);
router.patch('/me/password', userController.changeUserPassword);


module.exports = router;
