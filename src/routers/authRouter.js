const {Router} = require('express');
const router = Router();
const controllerAuth = require('../controllers/authController');

router.post('/register', controllerAuth.register);
router.post('/login', controllerAuth.login);
router.post('/forgot_password', controllerAuth.forgotPassword);

module.exports = router;
