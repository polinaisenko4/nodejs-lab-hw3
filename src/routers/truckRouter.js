const express = require('express');
const truckController = require('../controllers/truckController');

const router = express.Router();

router.get('/', truckController.getTrucks);
router.post('/', truckController.addTruck);
router.get('/:id', truckController.getTruckById);
router.put('/:id', truckController.updateTruckById);
router.delete('/:id', truckController.deleteTruckById);
router.post('/:id/assign', truckController.assignTruckById);

module.exports = router;
