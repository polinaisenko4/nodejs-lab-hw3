const checkShipperMiddleware = async (req, res, next) => {
  const {role} = req.user;
  console.log(role);

  if (role !== 'SHIPPER') {
    return res.status(400).send({
      message: 'This feature is only available to shippers.',
    });
  }
  next();
};

module.exports = checkShipperMiddleware;
