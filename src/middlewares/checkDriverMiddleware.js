const checkDriverMiddleware = async (req, res, next) => {
  const {role} = req.user;
  console.log(role);

  if (role !== 'DRIVER') {
    return res.status(400).send({
      message: 'This feature is only available to drivers.',
    });
  }
  next();
};

module.exports = checkDriverMiddleware;
