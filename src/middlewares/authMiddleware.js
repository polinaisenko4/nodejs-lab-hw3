const jwt = require('jsonwebtoken');

const authMiddleware = (req, res, next) => {
  try {
    const {authorization} = req.headers;
    console.log(authorization);

    if (!authorization) {
      return res.status(400)
          .send({message: 'Enter Authorization Headers'});
    }

    const token = authorization.split(' ')[1];

    if (!token) {
      return res.status(400)
          .send({message: 'Add token to request'});
    }

    const payload = jwt.verify(token, process.env.SECRET_KEY);

    req.user = {
      userId: payload._id,
      email: payload.email,
      role: payload.role,
    };
    console.log(payload);
    next();
  } catch (e) {
    res.status(400).send({message: e.message});
  }
};

module.exports = authMiddleware;
