if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  require('dotenv').config();
}

const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const logger = require('morgan');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const truckRouter = require('./routers/truckRouter');
const loadRouter = require('./routers/loadRouter');

const authMiddleware = require('./middlewares/authMiddleware');
const driverMiddleware = require('./middlewares/checkDriverMiddleware');


const PORT = process.env.PORT || 8080;

const app = express();

app.use(express.json());
app.use(cors());
app.use(logger('combined'));


app.use('/api/auth', authRouter);

app.use(authMiddleware);
app.use('/api/trucks', driverMiddleware, truckRouter);
app.use('/api/users', userRouter);
app.use('/api/loads', loadRouter);


app.use((req, res) => {
  res.status(404).send({message: 'Page not found'});
});

app.use(( req, res) => {
  res.status(500).send({message: 'Server error'});
});

const start = async () => {
  try {
    await mongoose.connect(process.env.DB_URL);
    app.listen(PORT,
        () => console.log(`Server running on ${PORT} port...`));
  } catch (err) {
    console.error(err.message);
  }
};

start().then(() => console.log('System started'));
